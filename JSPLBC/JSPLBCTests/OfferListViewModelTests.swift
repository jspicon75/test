//
//  OfferListViewModelTests.swift
//  JSPLeBonCoinCollectionTests
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import XCTest
@testable import JSPLBC

class OfferListViewModelTests: XCTestCase {

    func testOfferListViewModel() {
        let spyDelegate = SpyOfferListViewModelDelegate()
        let offerListViewModel = OfferListViewModel(spyDelegate)
        let myExpectation = expectation(description: "Expectaion fulfilled in delegate")
        spyDelegate.asyncExpectation = myExpectation

        // Test initial values
        XCTAssert(offerListViewModel.numberOfCategories() == 0)
        XCTAssert(offerListViewModel.numberOfOffers() == 0)

        offerListViewModel.getData()
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout error: \(error)")
            }

            XCTAssert(offerListViewModel.numberOfCategories() > 0)
            XCTAssert(offerListViewModel.numberOfOffers() > 0)
        }
    }

}

class SpyOfferListViewModelDelegate: NSObject, OfferListViewModelDelegate {

    // Setting .None is unnecessary, but helps with clarity imho
    var state: DataProviderState? = .stopped

    // Async test code needs to fulfill the XCTestExpecation used for the test
      // when all the async operations have been completed. For this reason we need
      // to store a reference to the expectation
    var asyncExpectation: XCTestExpectation?

    func dataRetrivedWithSuccess() {
        guard let expectation = asyncExpectation else {
          XCTFail("SpyDelegate was not setup correctly. Missing XCTExpectation reference")
          return
        }
        expectation.fulfill()
    }
}
