//
//  DataProviderTests.swift
//  JSPLeBonCoinCollectionTests
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import XCTest
@testable import JSPLBC

class DataProviderTests: XCTestCase {

    func testGetDataIfNeeded() {
        let dataProvider = MockDataProvider()
        let testExpectationConnected = expectation(description: "testGetDataConnected")
        dataProvider.networkMoniter = MockNetworkMoniterConnected()

        dataProvider.start()
        XCTAssert(dataProvider.state == .started)
        dataProvider.getDataIfNeeded { result in
            XCTAssertTrue(result)
            testExpectationConnected.fulfill()
        }
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

        let testExpectationDiconnected = expectation(description: "testGetDataDisconnected")
        dataProvider.networkMoniter = MockNetworkMoniterDisconnected()
        dataProvider.getDataIfNeeded { result in
            XCTAssertTrue(result)
            testExpectationDiconnected.fulfill()
        }
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

    }
    
    func testStateChange() {
        let dataProvider = DataProvider.shared
        let spyDelegate = SpyDataProviderDelegate()
        dataProvider.delegate = spyDelegate
        let myExpectation = expectation(description: "Expectaion fulfilled in delegate")
        spyDelegate.asyncExpectation = myExpectation

        dataProvider.start()

        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }

            guard let state = spyDelegate.state else {
                XCTFail("Expected delegate to be called")
                return
            }

            XCTAssert(state == .ready)
        }
    }
}

class SpyDataProviderDelegate: DataProviderStateChangeDelegate {
    // Setting .None is unnecessary, but helps with clarity imho
    var state: DataProviderState? = .stopped

    // Async test code needs to fulfill the XCTestExpecation used for the test
      // when all the async operations have been completed. For this reason we need
      // to store a reference to the expectation
    var asyncExpectation: XCTestExpectation?

    func stateDidChange(_ state: DataProviderState) {
        guard let expectation = asyncExpectation else {
          XCTFail("SpyDelegate was not setup correctly. Missing XCTExpectation reference")
          return
        }

        self.state = state
        if state == .ready {expectation.fulfill()}
    }
}

class MockDataProvider: DataProviderProtocol {
    func start() {
        state = .started
    }

    var state: DataProviderState = .stopped

    var networkMoniter: NetworkMoniterProtocol?

    func getDataIfNeeded(completion: @escaping (Bool) -> Void) {
        completion(true)
    }
}

class MockNetworkMoniterConnected: NetworkMoniterProtocol {
    var netOn: Bool = true
}

class MockNetworkMoniterDisconnected: NetworkMoniterProtocol {
    var netOn: Bool = false
}
