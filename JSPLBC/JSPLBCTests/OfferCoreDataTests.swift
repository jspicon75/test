//
//  OfferCoreDataTests.swift
//  JSPLeBonCoinCollectionTests
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import XCTest
@testable import JSPLBC
import CoreData

class OfferCoreDataTests: XCTestCase {
    var coreDataStack: CoreDataStorage!

    override func setUp() {
      super.setUp()
      coreDataStack = TestCoreDataStack()
    }

    override func tearDown() {
      super.tearDown()
      coreDataStack = nil
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAddOffer() {
        let offer = Offer(context: TestCoreDataStack.shared.managedObjectContext())
        offer.id = 1

        XCTAssertNotNil(offer, "Offer should not be nil")
        XCTAssertNotNil(offer.id, "id should not be nil")
    }

    func testJSONMappingForOffers() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "Offers", withExtension: "json") else {
            XCTFail("Missing file: Offer.json")
            return
        }
        let jsonDecoderlbc: JSONDecoderlbc = JSONDecoderlbc()
        let managedObjectContext = TestCoreDataStack.shared.managedObjectContext()
        jsonDecoderlbc.userInfo[CodingUserInfoKey.managedObjectContext!] = managedObjectContext
        let json = try Data(contentsOf: url)
        let offers = try jsonDecoderlbc.decode([Offer].self, from: json)

        XCTAssertEqual(offers.count, 2)
        XCTAssertEqual(offers.first!.id, 1461267313)
        XCTAssertEqual(offers.first!.categoryId, 4)
        XCTAssertEqual(offers.first!.title, "Title1")
        XCTAssertEqual(offers.first!.desc, "Description1")
    }

    func testJSONMappingForCategories() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "Categories", withExtension: "json") else {
            XCTFail("Missing file: Categories.json")
            return
        }
        let jsonDecoderlbc: JSONDecoderlbc = JSONDecoderlbc()
        let managedObjectContext = TestCoreDataStack.shared.managedObjectContext()
        jsonDecoderlbc.userInfo[CodingUserInfoKey.managedObjectContext!] = managedObjectContext
        let json = try Data(contentsOf: url)
        let categories = try jsonDecoderlbc.decode([JSPLBC.Category].self, from: json)

        XCTAssertEqual(categories.count, 11)
        XCTAssertEqual(categories.first!.name, "Véhicule")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
