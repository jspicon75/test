//
//  TestCoreDataStack.swift
//  JSPLeBonCoinCollectionTests
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation
import CoreData
import JSPLBC

class TestCoreDataStack: CoreDataStorage {
  override init() {
    super.init()

    let persistentStoreDescription = NSPersistentStoreDescription()
    persistentStoreDescription.type = NSInMemoryStoreType

    let container = NSPersistentContainer(name: "JSPLeBonCoinCollection")
    container.persistentStoreDescriptions = [persistentStoreDescription]

    container.loadPersistentStores { _, error in
      if let error = error as NSError? {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    }

    persistentContainer = container
  }
}
