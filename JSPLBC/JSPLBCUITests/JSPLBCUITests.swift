//
//  JSPLBCUITests.swift
//  JSPLBCUITests
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import XCTest

class JSPLBCUITests: XCTestCase {

    func testFirstCollectionCell() {
        let app = XCUIApplication()
        app.launch()
        // Filter in offers'list
        XCTAssertTrue(app.isDisplayingFilter)

        let firstChild = app.collectionViews.children(matching:.any).element(boundBy: 0)
        if firstChild.exists {
             firstChild.tap()
        }

        // No filter in detail view
        XCTAssertFalse(app.isDisplayingFilter)

    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}

extension XCUIApplication {
    var isDisplayingFilter: Bool {
        return XCUIApplication().buttons["Filter"].exists
    }
}
