//
//  CategoryCell.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

class CategoryCell: UITableViewCell {
    static let categoryCellIdentifier = "categoryCellIdentifier"

    func configure(_ categoryViewModel: CategoryViewModel?) {
        if let  categoryViewModel = categoryViewModel {
            categoryNameLabel.text = categoryViewModel.name
            categoryNameLabel.textColor = categoryViewModel.color
        } else {
            categoryNameLabel.text = "No filter"
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addViews(){
        backgroundColor = .clear
        addSubview(categoryNameLabel)
        categoryNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
        categoryNameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        categoryNameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        categoryNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }

    fileprivate let categoryNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:18.0)!
        label.textColor = .darkGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
}
