//
//  OfferCell.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

class OfferCell: UICollectionViewCell {
    static let offerCellIdentifier = "offerCellIdentifier"

    func configure(_ offerViewModel: OfferViewModel) {
        offerImageView.image = ImageCache.shared.placeholderImage
        offerViewModel.loadSmall { (image) in
            DispatchQueue.main.async {
                self.offerImageView.image = image
            }
        }
        categoryNameLabel.text = offerViewModel.categoryName
        offerTitleLabel.attributedText = offerViewModel.titleAttributed
        offerPriceLabel.text = offerViewModel.price
        offerDateLabel.text = offerViewModel.creationDate
    }

    override public func prepareForReuse() {
        offerImageView.image = ImageCache.shared.placeholderImage
    }

    deinit {
        offerImageView.image = nil
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addViews()
    }

    func addViews(){
        backgroundColor = .clear
        addSubview(offerImageView)
        offerImageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        offerImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        offerImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        offerImageView.heightAnchor.constraint(equalTo: offerImageView.widthAnchor, multiplier: 140/105).isActive = true

        addSubview(stackView)
        stackView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        stackView.topAnchor.constraint(equalTo: offerImageView.bottomAnchor, constant: 10).isActive = true
        stackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true

        stackView.addArrangedSubview(categoryNameLabel)
        stackView.addArrangedSubview(offerTitleLabel)
        stackView.addArrangedSubview(offerPriceLabel)
        stackView.addArrangedSubview(offerDateLabel)
        stackView.addArrangedSubview(separator)
//        addSubview(separator)
//        separator.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
//        separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 1).isActive = true
//        separator.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
//        separator.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate let categoryNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:12.0)!
        label.textColor = .red
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    fileprivate let offerTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:12.0)!
        label.textColor = .darkGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    fileprivate let offerPriceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:14.0)!
        label.textColor = .lightGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    fileprivate let offerDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:10.0)!
        label.textColor = .lightGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    fileprivate let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillProportionally
        return stackView
    }()

    fileprivate let offerImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12
        return iv
    }()

    fileprivate let separator: UIView = {
        let separator = UIView()
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.backgroundColor = .lightGray
        return separator
    }()
}
