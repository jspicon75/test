//
//  CloudRequestManager.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

class CloudRequestsManager {
    static var shared: CloudRequestsManager = CloudRequestsManager()
    static var userAgent: String = ""
    // create the eTag headers according to the match request:
    //  mutate == false: access request, will receive status code 304 if not changed
    //  mutate == true : modification request, will receive status code 428 if the eTag is not up-to-date (needs a refresh first)
    static func headers(forEtag eTag: String?, toMutate mutate: Bool) -> CloudRequestHeaders? {
        return eTag == nil ? nil : [(mutate ? "If-Match" : "If-None-Match"): eTag!]
    }

    private var isReady = false

    /// sets the CloudRequestsManager as ready to send requests
    public func start() {
        isReady = true
    }

    /// stops and prevent new requests to be sent
    public func reset() {
        isReady = false
    }

    // MARK: - Dispatch method
    // send the request by feed key (nammed feeds, from the discovery)
    public func send(requestOfType type: CloudRequestType, withValues values: [String: Any]? = nil, withHeaders headers: CloudRequestHeaders? = nil, withData data: Data? = nil, withCompletion completion: CloudRequestCompletion? = nil) {
        // get the request feed
        guard let feed = FeedsManager.shared.feed(with: type) else {
            print("[CloudRequestsManager] ERROR: no feed with type: \(type)")
            completion?(0, [:], nil)
            return
        }

        // create the cloud request
        guard let cloudRequest = CloudRequest(withFeed: feed, withCompletion: completion) else {
            print("[CloudRequestsManager] ERROR: failed to create the cloud request for the requested type: \(type)")
            completion?(0, [:], nil)
            return
        }

        cloudRequest.cachePolicy = .useProtocolCachePolicy

        // send the request
        send(cloudRequest: cloudRequest)
    }

    private func resume(cloudRequest: CloudRequest) {
        if !Thread.isMainThread { return DispatchQueue.main.async(execute: { self.resume(cloudRequest: cloudRequest) }) }

        send(cloudRequest: cloudRequest)
    }

    // MARK: - Send method
    // Final method to send requests. Directly using NSUrlSession.
    public func send(cloudRequest: CloudRequest) {
        // prevent requests to be sent while the manager is not ready
        guard isReady else { return }

        // request with the filled url
        var urlRequest = URLRequest(url: cloudRequest.url, cachePolicy: cloudRequest.cachePolicy, timeoutInterval: cloudRequest.timeoutInterval)

        // HTTP method
        urlRequest.httpMethod = cloudRequest.httpMethod

        // always add the User-Agent header
        urlRequest.setValue(CloudRequestsManager.userAgent, forHTTPHeaderField: "User-Agent")

        let urlSession: URLSession = URLSession.shared

        // create the data task
//        let requestLaunchDate = Date()
        urlSession.dataTask(with: urlRequest) { data, response, error in
            let httpResponse = response as? HTTPURLResponse
            let statusCode = httpResponse?.statusCode ?? 0

            // parse the response
            guard httpResponse != nil else {
                print("[CloudRequestsManager] ERROR: received nil response. Error: \(String(describing: error?.localizedDescription))")
                cloudRequest.completion?(0, [:], nil)
                return
            }

            // get the headers
            let responseHeaders = httpResponse?.allHeaderFields as? CloudRequestHeaders ?? [:]

            // handle errors
            if statusCode >= 400 {
                // log error
                print("[CloudRequestsManager] ERROR: received statusCode \(statusCode) for request:\n[\(cloudRequest.httpMethod)] \(cloudRequest.url.absoluteString)\nwith headers:\(cloudRequest.headers)")

                // get the response body
                guard data != nil else {
                    print("[CloudRequestsManager] ERROR: no data error: \(statusCode)")
                    cloudRequest.completion?(statusCode, responseHeaders, nil)
                    return
                }
            } else if statusCode == 0 {
                print("[CloudRequestsManager] ERROR: request ended with status code 0 (client-side error). Error: \(String(describing: error))")
                cloudRequest.completion?(statusCode, responseHeaders, nil)
                return
            }

            // if the response eTag is equal to the request one, the response comes from the cache. Simulate a 304 (not changed)
            let requestETag = cloudRequest.headers["If-None-Match"]
            if requestETag != nil && requestETag == responseHeaders["Etag"] {
                cloudRequest.completion?(304, responseHeaders, data)
                return
            }

            // Let's store Etag for feed if returned in heders
            if let requestETag = responseHeaders["Etag"], let httpResponseUrl = httpResponse?.url {
                if let feed = FeedsManager.shared.feed(from: httpResponseUrl) {
                    feed.eTag = requestETag
                }
            }

            // everything is fine
            cloudRequest.completion?(statusCode, responseHeaders, data)
        }.resume()
    }
}
