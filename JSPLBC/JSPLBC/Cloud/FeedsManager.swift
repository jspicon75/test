//
//  FeedsManager.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

class FeedsManager {
    static let shared = FeedsManager()
    private init() {}
    var feeds: [CloudRequestType: Feed] = [:]

    public func feed(with type: CloudRequestType) -> Feed? {
        return feeds[type]
    }

    public func feed(from url: URL) -> Feed? {
        return feeds.first(where:{ $0.value.url.absoluteString == url.absoluteString})?.value
    }

    public func resetFeeds() {
        feeds = [:]
    }
}
