//
//  CloudRequest.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

class CloudRequest {
    // cloud request parameters
    var httpMethod: String
    var url: URL
    var headers: CloudRequestHeaders
    var urlSessionId: String? = nil
    var completion: CloudRequestCompletion? = nil

    // URLRequest parameters
    var cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
    var timeoutInterval: TimeInterval = 15.0


    // MARK: - Init
    // create a cloud request with given URLRequest parameters
    init(withUrl inUrl: URL, withMethod inMethod: String? = nil, withHeaders inHeaders: CloudRequestHeaders?, withCompletion inCompletion: CloudRequestCompletion? = nil) {
        url = inUrl
        httpMethod = inMethod ?? "GET"
        headers = CloudRequest.headers(forMethod: inMethod).merging(inHeaders ?? [:]) { _ , new in new }
        completion = inCompletion
    }

    // create a cloud request from the given feeds
    convenience init?(withFeed inFeed: Feed, withCompletion inCompletion: CloudRequestCompletion? = nil) {
        var headers: CloudRequestHeaders = [:]
        if let eTag = inFeed.eTag {
            headers = ["If-None-Match": eTag]
        }
        self.init(withUrl: inFeed.url, withMethod: inFeed.method, withHeaders: headers, withCompletion: inCompletion)
    }

    // MARK: - Class properties and methods
    static var defaultHeaders: CloudRequestHeaders = ["Accept-Encoding": "gzip"]

    static private func headers(forMethod httpMethod: String?) -> CloudRequestHeaders {
        var headers: CloudRequestHeaders = CloudRequest.defaultHeaders
        switch httpMethod {
        case "GET": break
        default: headers["Content-Type"] = "application/json"
        }
        return headers
    }
}


// MARK: - CustomStringConvertible extension
extension CloudRequest: CustomDebugStringConvertible {
    public var debugDescription: String {

        return "[CloudRequest] [\(httpMethod)] \(url)\n  Headers: \(String(describing: headers))"
    }
}
