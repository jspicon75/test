//
//  CloudRequestType.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

typealias CloudRequestHeaders = [String: String]
typealias CloudRequestCompletion = (_ statusCode: Int, _ responseHeaders: CloudRequestHeaders, _ data: Data?) -> Void

enum CloudRequestType: String {
    case offers
    case categories
    case image
}
