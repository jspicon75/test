//
//  NetworkMoniter.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation
import Network

protocol NetworkMoniterProtocol {
    var netOn: Bool { get }
}

let networkStatusDidChangeNotification = Notification.Name("networkStatusDidChange")

@available(iOS 12.0, *)
class NetworkMoniter: NetworkMoniterProtocol {
    static public let shared = NetworkMoniter()
    private var monitor: NWPathMonitor
    private var queue = DispatchQueue.global()
    var netOn: Bool = true {
        didSet {
            NotificationCenter.default.post(name: networkStatusDidChangeNotification, object: nil, userInfo: nil)
        }
    }

    private init() {
        self.monitor = NWPathMonitor()
        self.queue = DispatchQueue.global(qos: .background)
        self.monitor.start(queue: queue)
    }

    func startMonitoring() {
        self.monitor.pathUpdateHandler = { path in
            self.netOn = path.status == .satisfied
        }
    }

    func stopMonitoring() {
        self.monitor.cancel()
    }
}
