//
//  DataProvider.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

typealias SyncTaskResult = Result<SyncTaskSuccess, SyncTaskFailure>

enum SyncTaskSuccess {
    case ok
}

enum SyncTaskFailure: Error, CustomStringConvertible {
    case fatal
    case decoder
    var description: String  {
        switch self {
        case .fatal: return "Bad status code in request"
        case .decoder: return "Error decodeing data"
        }
    }
}
enum DataProviderState {
    case started
    case syncing
    case loading
    case ready
    case stopped
    case error
}

protocol DataProviderStateChangeDelegate: AnyObject {
    func stateDidChange(_ state: DataProviderState)
}

protocol DataProviderProtocol {
    var networkMoniter: NetworkMoniterProtocol? { get set }
    var state: DataProviderState{ get }
    func start()
    func getDataIfNeeded(completion: @escaping (_ success: Bool) -> Void)
}

class DataProvider: DataProviderProtocol {
    static public let shared = DataProvider()
    weak var delegate: DataProviderStateChangeDelegate?
    var networkMoniter: NetworkMoniterProtocol?

    var state: DataProviderState = .stopped {
        didSet {
            if state == oldValue { return }
            print("DataProviderState changed to \(state)")
            delegate?.stateDidChange(state)
        }
    }

    var offers: [Offer] = []
    var categories: [Category] = []

    private init() {
        let feedListOffers = Feed(with: URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json")!)
        FeedsManager.shared.feeds[.offers] = feedListOffers
        let feedCategories = Feed(with: URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json")!)
        FeedsManager.shared.feeds[.categories] = feedCategories
        NotificationCenter.default.addObserver(self, selector: #selector(networkAvailibilityChanged), name: networkStatusDidChangeNotification, object: nil)
        CloudRequestsManager.shared.start()
        if #available(iOS 12.0, *) {
            networkMoniter = NetworkMoniter.shared
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func start() {
        state = .started

        getData()
    }

    @objc func networkAvailibilityChanged() {
        if #available(iOS 12.0, *) {
            if NetworkMoniter.shared.netOn {
                state == .stopped ? start() : refreshData()
            }
        }
    }

    func refreshData() {
        getData()
    }

    private func getData() {
        print("[DataProvider]: start getting data if needed")
        getDataIfNeeded { success in
            print("[DataProvider]: end getting data")
            if !success {
                print("error while getting data")
                self.state = .error
            }
            self.state = .ready
        }
    }

    func stop() {
        self.state = .stopped
    }

    func loadData() {
        self.state = .loading
        CoreDataStorage.shared.load()
    }

    var isDownlodRequiredAndAvailable: Bool {
        if #available(iOS 12.0, *) {
            return NetworkMoniter.shared.netOn
        } else {
            return true
        }
    }

    func getDataIfNeeded(completion: @escaping (_ success: Bool) -> Void) {
        // Need to check if
        // - data really need to be synced via Expires date from header
        // - Network is available
        if isDownlodRequiredAndAvailable {
            state = .syncing
            // get datas
            var globalSuccess = true
            let dispatchGroup = DispatchGroup()

            //Offers part
            dispatchGroup.enter()
            getAndStoreOffers { (result) in
                switch result {
                case .failure(let error):
                    print("Error : \(error.description)")
                    globalSuccess = false
                case .success(_):
                    break
                }
                dispatchGroup.leave()
            }

            // Categories part
            dispatchGroup.enter()
            getAndStoreCategories { (result) in
                switch result {
                case .failure(let error):
                    print("Error : \(error.description)")
                    globalSuccess = false
                case .success(_):
                    break
                }
                dispatchGroup.leave()
            }
            dispatchGroup.notify(queue: .global()) {
                if globalSuccess {
                    completion(true)
                } else {
                    self.state = .error
                    completion(false)
                }
            }
        } else {
            loadData()
            categories = CoreDataStorage.shared.loadedCategories
            offers = CoreDataStorage.shared.loadedOffers
            state = .ready
            completion(true)
        }
    }

    private func getAndStoreOffers(_ completion: @escaping (_ result: SyncTaskResult) -> Void) {
        CloudRequestsManager.shared.send(requestOfType: .offers) { statusCode, responseHeaders, data in

            guard statusCode >= 200 && statusCode <= 304 && data != nil else {
                print("[DataProvider] ERROR: received an empty or unexpected response for offers")
                    completion(.failure(.fatal))
                return
            }
            // Data are not modified since last load.
            // Do not update local mode and keep usin it
            if statusCode == 304 {
                completion(.success(.ok))
                return
            }
            let jsonDecoderlbc: JSONDecoderlbc = JSONDecoderlbc()
            let managedObjectContext = CoreDataStorage.shared.managedObjectContext()
            guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
                fatalError("Failed to retrieve managed object context Key")
            }
            jsonDecoderlbc.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
            do {
                let offers = try jsonDecoderlbc.decode([Offer].self, from: data!)
                self.offers = offers
                completion(.success(.ok))
            } catch {
                print("[DataProvider] ERROR: offers decode error")
                completion(.failure(.decoder))
            }
        }
    }

    private func getAndStoreCategories(_ completion: @escaping (_ result: SyncTaskResult) -> Void) {
        CloudRequestsManager.shared.send(requestOfType: .categories) { statusCode, responseHeaders, data in

            guard statusCode >= 200 && statusCode <= 304 && data != nil else {
                print("[DataProvider] ERROR: received an empty or unexpected response for offers")
                completion(.failure(.fatal))
                return
            }
            // Data are not modified since last load.
            // Do not update local mode and keep usin it
            if statusCode == 304 {
                completion(.success(.ok))
                return
            }
            let jsonDecoderlbc: JSONDecoderlbc = JSONDecoderlbc()
            let managedObjectContext = CoreDataStorage.shared.managedObjectContext()
            guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
                fatalError("Failed to retrieve managed object context Key")
            }
            jsonDecoderlbc.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
            do {
                let categories = try jsonDecoderlbc.decode([Category].self, from: data!)
                self.categories = categories
                completion(.success(.ok))
            } catch {
                print("[DataProvider] ERROR: categories decode error")
                completion(.failure(.decoder))
            }
        }
    }

}
