//
//  Feed.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

class Feed {
    var method: String = "GET"
    let url: URL
    var eTag: String? = ""

    // MARK: - Initializers
    init(with url: URL) {
        self.url = url
    }
}
