//
//  CoreDataStorage.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit
import CoreData

open class CoreDataStorage: NSObject {
    public static let shared = CoreDataStorage()
    private var _offers: [Offer] = []
    var loadedOffers: [Offer] {
        return _offers
    }
    private var _categories: [Category] = []
    var loadedCategories: [Category] {
        return _categories
    }

    enum CoreDataStorageElementType: String {
        case offer
        case category
        public var entityName: String {
            switch self {
            case .offer:
                return "Offer"
            case .category:
                return "Category"
            }
        }
    }

    // MARK: - Core Data stack

    public lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "JSPLBC")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("[DataManager] : ERROR loadPersistant store unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    func managedObjectContext() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    // MARK: - Core Data Saving support
    func deleteEntity(of type: CoreDataStorageElementType) {
        let isInMemoryStore = persistentContainer.persistentStoreDescriptions.reduce(false) {
            return $0 ? true : $1.type == NSInMemoryStoreType
        }

        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: type.entityName)
        // NSBatchDeleteRequest is not supported for in-memory stores
        if isInMemoryStore {
            do {
                let entities = try managedObjectContext.fetch(fetchRequest)
                for entity in entities {
                    managedObjectContext.delete(entity as! NSManagedObject)
                }
            } catch let error as NSError {
                print(error)
            }
        } else {
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try managedObjectContext.execute(batchDeleteRequest)
            } catch let error as NSError {
                print(error)
            }
        }
    }

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("[DataManager] : ERROR saveContext unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    // MARK: - Core Data Loading support
    func load() {
        loadCategories()
        loadOffers()
    }

    private func loadCategories(){
        print("[DataManager: start loading categories")
//        var categories = [Category]()
        let fetchRequest = NSFetchRequest<Category>(entityName: CoreDataStorageElementType.category.entityName)
        do {
            let categories = try self.persistentContainer.viewContext.fetch(fetchRequest)
            print("[DataManager] : loadCategories count :  \(categories.count)")
            _categories = categories
        } catch let error as NSError {
            print("[DataManager] : ERROR load \(CoreDataStorageElementType.category.entityName) error : \(error), \(error.userInfo)")
        }
        print("[DataManager: end loading categories")
    }

    private func loadOffers() {
        print("[DataManager: start loading offers")
        let fetchRequest = NSFetchRequest<Offer>(entityName: CoreDataStorageElementType.offer.entityName)
        do {
            let offers = try self.persistentContainer.viewContext.fetch(fetchRequest)
            _offers = offers
            print("[DataManager] : loadOffers count :  \(offers.count)")
        } catch let error as NSError {
            print("[DataManager] : ERROR load \(CoreDataStorageElementType.category.entityName) error : \(error), \(error.userInfo)")
        }
        print("[DataManager: end loading offers")
    }
}
