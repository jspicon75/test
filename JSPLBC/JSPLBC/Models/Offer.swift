//
//  Offer.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation
import CoreData

class Offer: NSManagedObject, Codable {
    @NSManaged var id: Int64
    @NSManaged var categoryId: Int64
    @NSManaged var title: String
    @NSManaged var desc: String
    @NSManaged var price: Double
    @NSManaged var smallImagesUrl: String?
    @NSManaged var thumbImagesUrl: String?
    @NSManaged var creationDate: Date
    @NSManaged var isUrgent: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case categoryId     = "category_id"
        case title
        case description
        case price
        case imagesUrl      = "images_url"
        case creationDate   = "creation_date"
        case isUrgent       = "is_urgent"
    }

    enum ImagesUrlCodingKeys: String, CodingKey {
         case small
         case thumb
    }

    static func == (lhs: Offer, rhs: Offer) -> Bool {
        return lhs.id == rhs.id
    }

    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Offer", in: managedObjectContext) else {
            fatalError("Failed to decode Offer")
        }

        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int64.self, forKey: .id)
        categoryId = try container.decode(Int64.self, forKey: .categoryId)
        title = try container.decode(String.self, forKey: .title)
        desc = try container.decode(String.self, forKey: .description)
        price = try container.decode(Double.self, forKey: .price)
        let imageUrls = try container.nestedContainer(keyedBy: ImagesUrlCodingKeys.self, forKey: .imagesUrl)
        smallImagesUrl = try imageUrls.decodeIfPresent(String.self, forKey: .small)
        thumbImagesUrl = try imageUrls.decodeIfPresent(String.self, forKey: .thumb)
        creationDate = try container.decode(Date.self, forKey: .creationDate)
        isUrgent = try container.decode(Bool.self, forKey: .isUrgent)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(categoryId, forKey: .categoryId)
        try container.encode(title, forKey: .title)
        try container.encode(desc, forKey: .description)
        try container.encode(price, forKey: .price)
        var imagesUrls = container.nestedContainer(keyedBy: ImagesUrlCodingKeys.self, forKey: .imagesUrl)
        try imagesUrls.encode(smallImagesUrl, forKey: .small)
        try imagesUrls.encode(thumbImagesUrl, forKey: .thumb)
        try container.encode(creationDate, forKey: .creationDate)
        try container.encode(isUrgent, forKey: .isUrgent)
    }
}
