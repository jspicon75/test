//
//  OfferListViewModel.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation
import UIKit

protocol OfferListViewModelDelegate: NSObject {
    func dataRetrivedWithSuccess()
}

class OfferListViewModel {
    weak var delegate: OfferListViewModelDelegate?

    private var offers: [Offer]
    private var categories: [Category]

    init(_ delegate: OfferListViewModelDelegate?) {
        self.delegate = delegate
        self.offers = [Offer]()
        self.categories = [Category]()
    }

    func getData() {
        DataProvider.shared.delegate = self
        DataProvider.shared.start()
    }

    func refreshData() {
        DataProvider.shared.refreshData()
    }

    func reloadOffers(_ category: Category? = nil) {
        prepareData(category)
    }

    private func prepareData(_ category: Category? = nil) {
        var filteredAndOrderedOffers = DataProvider.shared.offers.filter { $0.isUrgent }.sorted { $0.creationDate.compare($1.creationDate) == ComparisonResult.orderedAscending }
        filteredAndOrderedOffers += DataProvider.shared.offers.filter { !$0.isUrgent }.sorted { $0.creationDate.compare($1.creationDate) == ComparisonResult.orderedAscending }
        if let category = category {
            self.offers = filteredAndOrderedOffers.filter { $0.categoryId == category.id }
        } else {
            self.offers = filteredAndOrderedOffers
        }
        self.categories =  DataProvider.shared.categories.sorted {
            $0.id < $1.id }

        self.delegate?.dataRetrivedWithSuccess()
    }

    func numberOfCategories() -> Int {
        return self.categories.count
    }

    func category(atIndex index: Int) -> CategoryViewModel? {
        if index < numberOfCategories() {
            return CategoryViewModel(self.categories[index])
        }
        return nil
    }

    func numberOfOffers() -> Int {
        return self.offers.count
    }

    func offer(atIndex index: Int) -> OfferViewModel {
        let offer = self.offers[index]
        return OfferViewModel(offer, category(of: offer))
    }

    private func category(of offer: Offer) -> Category? {
        categories.first(where:{ $0.id == offer.categoryId})
    }
}

extension OfferListViewModel: DataProviderStateChangeDelegate {
    func stateDidChange(_ state: DataProviderState) {
        switch state {
        case .ready:
            prepareData()
        case .error:
            print("show error")
        case .stopped: // Not used
            print("show stopped")
        case .loading, .started, .syncing:
            print("show loading")
        }
    }
}
