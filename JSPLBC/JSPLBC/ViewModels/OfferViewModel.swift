//
//  OfferViewModel.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation
import UIKit

class OfferViewModel {
    private var offer: Offer
    private var category: Category?
    private var dateFormatter: DateFormatter
    private var priceFormatter: NumberFormatter

    init(_ offer: Offer, _ category: Category?) {
        self.offer = offer
        self.category = category
        self.dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        dateFormatter.doesRelativeDateFormatting = true
        priceFormatter = NumberFormatter()
        priceFormatter.numberStyle = .currency
        priceFormatter.currencyCode = "EUR"
    }

    var id: Int64 {
        get {
            return self.offer.id
        }
    }

    var categoryId: Int64 {
        get {
            return self.offer.categoryId
        }
    }

    var categoryName: String {
        get {
            guard let category = category else { return "" }
            return category.name
        }
    }

    var title: String {
        get {
            if (self.offer.title.count > 20) {
              return self.offer.title.prefix(17) + " …";
            } else {
              return self.offer.title;
            }
        }
    }

    var titleAttributed: NSMutableAttributedString {
        get {
            let textFont = UIFont(name: "HelveticaNeue", size: 14)!
            let attributes: [NSAttributedString.Key : Any] = [.font: textFont, .foregroundColor: UIColor.gray]
            let titleText = NSMutableAttributedString()
            if self.isUrgent {
                let image = UIImage(named: "urgent")!
                let size = CGSize(width: 20, height: 20)
                let yOffset = textFont.capHeight / 2 - size.height / 2
                let image1String = NSAttributedString.attributedString(with: image, at: yOffset, with: size)
                titleText.append(image1String)
            }
            let title = self.offer.title
            titleText.append(NSAttributedString(string: title, attributes: attributes))
            return titleText
        }
    }

    var desc: String {
        get {
            return self.offer.desc
        }
    }

    var price: String {
        get {
            return priceFormatter.string(from: NSNumber(value: self.offer.price)) ?? ""
        }
    }

    var smallImagesUrl: URL? {
        get {
            if let smallUrl = self.offer.smallImagesUrl {
                return URL(string: smallUrl)
            }
            return nil
        }
    }

    private var thumbImagesUrl: URL? {
        get {
            if let thumbUrl = self.offer.thumbImagesUrl {
                return URL(string: thumbUrl)
            }
            return nil
        }
    }

    var creationDate: String {
        get {
            return "\(dateFormatter.string(from: self.offer.creationDate))"
        }
    }

    private var isUrgent: Bool {
        get {
            return self.offer.isUrgent
        }
    }

    func loadThumb(completion: @escaping(UIImage?) -> Void) {
        loadImage(with: thumbImagesUrl, completion: completion)
    }

    func loadSmall(completion: @escaping(UIImage?) -> Void) {
        loadImage(with: smallImagesUrl, completion: completion)
    }

    private func loadImage(with imageUrl:URL?, completion: @escaping(UIImage) -> Void) {
        if #available(iOS 12.0, *) {
            if !NetworkMoniter.shared.netOn { return }
        }
        guard let url = imageUrl else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data else { return }
            if let image = UIImage(data: data) {
                completion(image)
            }
        }.resume()
    }
}
