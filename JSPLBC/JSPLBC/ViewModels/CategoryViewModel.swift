//
//  CategoryViewModel.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation
import UIKit

class CategoryViewModel {
    private var category: Category
    init(_ category: Category) {
        self.category = category
    }

    var id: Int64 {
        get {
            return self.category.id
        }
    }

    var name: String {
        get {
            return self.category.name
        }
    }

    var color: UIColor {
        get {
            return UIColor.random
        }
    }

    func getCategory() -> Category {
        return category
    }
}
