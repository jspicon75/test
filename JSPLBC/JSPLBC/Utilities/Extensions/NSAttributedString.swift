//
//  NSAttributedString.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

extension NSAttributedString {
    public class func attributedString(with image: UIImage, at yOffset: CGFloat, with size: CGSize? = nil) -> NSAttributedString {
        let size = size ?? image.size
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: yOffset, width: size.width, height: size.height)
        return NSAttributedString(attachment: attachment)
    }
}
