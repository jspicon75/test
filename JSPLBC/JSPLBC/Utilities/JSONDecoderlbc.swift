//
//  JSONDecoderlbc.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import Foundation

class JSONDecoderlbc: JSONDecoder {
    override init() {
        super.init()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateDecodingStrategy = .formatted(dateFormatter)
    }
}
