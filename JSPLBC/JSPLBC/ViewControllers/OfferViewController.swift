//
//  OfferViewController.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

class OfferViewController: UIViewController {
    var offerViewModel: OfferViewModel!

    convenience init() {
        fatalError("Not implemented")
    }

    init(_ offerViewModel: OfferViewModel) {
        self.offerViewModel = offerViewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(named: "background")
        populate(self.offerViewModel)
        self.title = offerViewModel.title
    }

    override func loadView() {
        super.loadView()
        navigationController?.navigationBar.barTintColor = UIColor(named: "background")
        createViews()
    }

    func populate(_ offerViewModel: OfferViewModel) {
        offerImageView.image = ImageCache.shared.placeholderImage
        offerViewModel.loadThumb { (image) in
            DispatchQueue.main.async {
                self.offerImageView.image = image
            }
        }
        offerTitleLabel.attributedText = offerViewModel.titleAttributed
        offerPriceLabel.text = offerViewModel.price
        offerDescriptionLabel.text = offerViewModel.desc
        offerCreationDateLabel.text = offerViewModel.creationDate
    }

    func createViews() {
        view.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        let insets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        scrollView.contentInset = insets
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 23.0
        scrollView.addSubview(stackView)
        let metrics = ["marginL": 12, "marginR": 12]
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-marginL-[stackView]-marginR-|", options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: metrics, views: ["stackView": stackView]))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-marginL-[stackView]-marginR-|", options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: metrics, views: ["stackView": stackView]))
        stackView.distribution = .equalSpacing
        offerImageView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(offerImageView)
        offerImageView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        offerImageView.widthAnchor.constraint(equalToConstant: view.frame.size.width-24).isActive = true
        offerImageView.heightAnchor.constraint(equalTo: offerImageView.widthAnchor, multiplier: 300/375).isActive = true

        stackView.addArrangedSubview(offerTitleLabel)
        stackView.addArrangedSubview(offerPriceLabel)
        stackView.addArrangedSubview(offerDescriptionLabel)
        stackView.addArrangedSubview(offerCreationDateLabel)
    }

    private var scrollView = UIScrollView()

    fileprivate let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fill
        return stackView
    }()

    fileprivate let offerImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12
        return iv
    }()

    fileprivate let offerTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:14.0)!
        label.textColor = .darkGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    fileprivate let offerDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:12.0)!
        label.textColor = .darkGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    fileprivate let offerPriceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:16.0)!
        label.textColor = .darkGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    fileprivate let offerCreationDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"Helvetica", size:12.0)!
        label.textColor = .darkGray
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
}
