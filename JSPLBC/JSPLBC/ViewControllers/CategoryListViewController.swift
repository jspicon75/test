//
//  CategoryListViewController.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

protocol CategoryListViewControllerDelegate: NSObject {
    func didSelectCategory(viewController: UIViewController, categoryViewModel: CategoryViewModel?)
}

class CategoryListViewController: UIViewController  {
    var offerListViewModel: OfferListViewModel!
    fileprivate var categoriesTableView: UITableView! = nil
    var categoriesTableViewDatasource: CategoryListViewControllerTableDatasource!

    weak var delegate: CategoryListViewControllerDelegate?

    convenience init() {
        fatalError("Not implemented")
    }

    init(_ offerListViewModel: OfferListViewModel) {
        self.offerListViewModel = offerListViewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        print("CategoryListViewController")

        setupTableView()

        self.categoriesTableViewDatasource = CategoryListViewControllerTableDatasource(self.offerListViewModel)
        self.categoriesTableView.dataSource = self.categoriesTableViewDatasource
        self.categoriesTableView.delegate = self

    }

    func setupTableView() {
        categoriesTableView = UITableView(frame: .zero)
        categoriesTableView.translatesAutoresizingMaskIntoConstraints = false
        categoriesTableView.register(CategoryCell.self, forCellReuseIdentifier: CategoryCell.categoryCellIdentifier)
        self.view.addSubview(categoriesTableView)
        categoriesTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        categoriesTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        categoriesTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        categoriesTableView.heightAnchor.constraint(equalToConstant: view.frame.height - 0).isActive = true
        categoriesTableView.separatorStyle = .none
        categoriesTableView.rowHeight = 60
        categoriesTableView.backgroundColor = UIColor(named: "background")
    }
}

extension CategoryListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectCategory(viewController: self, categoryViewModel: offerListViewModel.category(atIndex: indexPath.row))
    }
}
