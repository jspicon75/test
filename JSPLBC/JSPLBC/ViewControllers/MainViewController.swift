//
//  MainViewController.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad

class MainViewController: UIViewController {
    var offerListViewModel: OfferListViewModel!
    fileprivate var offersCollectionView: UICollectionView! = nil
    var offersCollectionViewDatasource: OffersListViewControllerCollectionDatasource!
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Nos annonces"
        navigationController?.navigationBar.barTintColor = UIColor(named: "background")
        self.view.backgroundColor = UIColor(named: "background")
        let filter = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(showCategorySelector))
        filter.accessibilityLabel = "Filter"

        navigationItem.rightBarButtonItems = [filter]

        setupCollectionView()

        self.offerListViewModel = OfferListViewModel(self)
        self.offersCollectionViewDatasource = OffersListViewControllerCollectionDatasource(self.offerListViewModel)
        self.offersCollectionView.dataSource = self.offersCollectionViewDatasource
        self.offersCollectionView.delegate = self
        self.offerListViewModel.getData()
    }

    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let divider: CGFloat = IS_IPAD ? 4 : 2
        let width = self.view.frame.width/divider - 20
        let height = width*2
        layout.itemSize = CGSize(width: width, height: height)
        offersCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        offersCollectionView.translatesAutoresizingMaskIntoConstraints = false
        offersCollectionView.register(OfferCell.self, forCellWithReuseIdentifier: OfferCell.offerCellIdentifier)
        self.view.addSubview(offersCollectionView)

        // Should change text, depending of validity of data
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Data ...", attributes: nil)

        refreshControl.tintColor = UIColor.red
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        offersCollectionView.refreshControl = refreshControl
        offersCollectionView.backgroundColor = .clear

        offersCollectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        offersCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        offersCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        offersCollectionView.heightAnchor.constraint(equalToConstant: view.frame.height - 0).isActive = true
    }

    @objc func refreshData() {
        print("refreshData")
        offerListViewModel.refreshData()
        refreshControl.endRefreshing()
    }

    @objc func showCategorySelector() {
        let categoryListViewController = CategoryListViewController(offerListViewModel)
        categoryListViewController.delegate = self
        present(categoryListViewController, animated: true, completion: nil)
    }
}

extension MainViewController: OfferListViewModelDelegate {
    func dataRetrivedWithSuccess() {
        print("[MainViewController] dataRetrivedWithSuccess")
        DispatchQueue.main.async(execute: {
            self.offersCollectionView.reloadData()
            self.offersCollectionView.setContentOffset(CGPoint(x: 0, y: -90), animated: true)
        })
    }
}

extension MainViewController: CategoryListViewControllerDelegate {
    func didSelectCategory(viewController: UIViewController, categoryViewModel: CategoryViewModel?) {
        if let categoryViewModel = categoryViewModel {
            print("filtered category : \(categoryViewModel.name)")
            self.offerListViewModel.reloadOffers(categoryViewModel.getCategory())
        } else {
            print("No filtered category ")
            self.offerListViewModel.reloadOffers()
        }
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected Cell: \(indexPath.row)")
        let offerViewModel = self.offerListViewModel.offer(atIndex: indexPath.row)
        let offerViewController = OfferViewController(offerViewModel)
        navigationController?.pushViewController(offerViewController, animated: true)
    }
}
