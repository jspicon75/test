//
//  OffersListViewControllerCollectionDatasource.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

class OffersListViewControllerCollectionDatasource: NSObject, UICollectionViewDataSource {
    let offerListViewModel: OfferListViewModel

    init(_ offerListViewModel: OfferListViewModel) {
        self.offerListViewModel = offerListViewModel
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offerListViewModel.numberOfOffers()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OfferCell.offerCellIdentifier, for: indexPath) as! OfferCell

        let offerViewModel = self.offerListViewModel.offer(atIndex: indexPath.row)
        cell.configure(offerViewModel)

        return cell
    }
}
