//
//  CategoryListViewControllerTableDatasource.swift
//  JSPLBC
//
//  Created by Jean-Sébastien PICON on 15/02/2021.
//

import UIKit

class CategoryListViewControllerTableDatasource: NSObject, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerListViewModel.numberOfCategories() + 1 // +1 for no selected category
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.categoryCellIdentifier, for: indexPath) as! CategoryCell
            let categoryViewModel = self.offerListViewModel.category(atIndex: indexPath.row)
            cell.configure(categoryViewModel)
        return cell
    }

    let offerListViewModel: OfferListViewModel

    init(_ offerListViewModel: OfferListViewModel) {
        self.offerListViewModel = offerListViewModel
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offerListViewModel.numberOfOffers()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OfferCell.offerCellIdentifier, for: indexPath) as! OfferCell

        let offerViewModel = self.offerListViewModel.offer(atIndex: indexPath.row)
        cell.configure(offerViewModel)

        return cell
    }
}
